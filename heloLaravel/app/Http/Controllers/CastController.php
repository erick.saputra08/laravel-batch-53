<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        // form validation 
        $request->validate([
            'name' => 'required | min:3 | max:100',
            'umur' => 'required|numeric | min:1 | max:100',
            'bio' => 'required | min:5'
        ]);
        
        // create new cast
        DB::table('cast')->insert([
            'name' => $request->name,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('/cast')->with('status', 'Data Cast Berhasil Ditambahkan!');
    }

    public function index() {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id) {
        // return $id;
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id) {
        $castById = DB::table('cast')->find($id);
        return view('cast.edit', ['castById' => $castById]);
    }


    public function update($id, Request $request) {
        $request->validate([
            'name' => 'required | min:3 | max:100',
            'umur' => 'required|numeric | min:1 | max:100',
            'bio' => 'required | min:5'
        ]);
       
        DB::table('cast')
        ->where('id', $id)
        ->update([
            'name' => $request["name"],
            'umur' => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('status', 'Data Cast Berhasil Diubah!');
    }

    // delete data cast
    public function destroy($id) {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('status', 'Data Cast Berhasil Dihapus!');
    }


}
