@extends('layouts.master')

@section('judul', 'Register')

@section('content')

    <form action="/welcome" method="POST">
        @csrf


        <label for="">First Name:</label> <br>
        <input type="text" name="fname"> <br> <br>

        <label for="">Last Name:</label> <br>
        <input type="text" name="lname"> <br> <br>

        <p>
            Gender: <br>
        </p>

        <input type="radio" id="man" name="fav_language" value="Man">
        <label for="man">Man</label><br>
        <input type="radio" id="woman" name="fav_language" value="Woman">
        <label for="woman">Woman</label><br>
        <input type="radio" id="Other" name="fav_language" value="Other">
        <label for="Other">Other</label>

        <br>
        <br>

        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="arab">Arab</option>
            <option value="england">England</option>
          </select>

          <br>
          <br>

          <p>
            Langue spoken: 
          </p>
        <input type="checkbox" id="bahasa" name="bahasa" value="Bike">
            <label for="bahasa"> Bahasa Indonesia</label><br>

        <input type="checkbox" id="japan" name="japan" value="Car">
            <label for="japan"> Japan</label><br>

        <input type="checkbox" id="english" name="english" value="Boat">
            <label for="english"> English</label><br><br>

            <br>
            <br>

            <p><label for="bio">Bio</label></p>
            <textarea id="bio" name="bio" rows="4" cols="50"></textarea></textarea>
            <br>


        <button type="submit">Send</button>
    </form>
    @endsection
    @push('scripts')
    <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
    @endpush
    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
    @endpush