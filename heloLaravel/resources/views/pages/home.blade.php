@extends('layouts.master')

@section('judul', 'Home')

@section('content')


    <h1>Selamat datang {{ $namaDepan }} {{ $namaBelakang }}</h1>

    <h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>

@endsection
@push('scripts')
<script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

