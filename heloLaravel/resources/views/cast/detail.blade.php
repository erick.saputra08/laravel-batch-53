@extends('layouts.master')

@section('judul', 'Detail Cast')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="/cast" class="btn btn-primary btn-sm my-3">Go Back</a>
            Name: {{ $cast->name }}
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>
                    Biodata:{{ $cast->bio }}
                </p>
                <footer class="blockquote-footer">
                    <cite title="Source Title">Umur: {{ $cast->umur }}</cite>
                </footer>
            </blockquote>
        </div>
    </div>

@endsection
