@extends('layouts.master')

@section('judul', 'Edit Cast')

@section('content')

    <form action="/cast/{{ $castById->id }}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" aria-describedby="name"
                placeholder="Enter name" name="name" value="{{ $castById->name }}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <label for="umur">Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" aria-describedby="umur"
                placeholder="Enter umur" name="umur" value="{{ $castById->umur }}">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <label for="bio">Biodata</label>
            <input type="text" class="form-control @error('bio') is-invalid @enderror" aria-describedby="bio"
                placeholder="Enter bio" name="bio" value="{{ $castById->bio }}">
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection
