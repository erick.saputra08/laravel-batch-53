@extends('layouts.master')

@section('judul', 'Table')

@section('content')

    <table id="example1" class="table table-bordered table-striped">
        <a href="/cast/create" class="btn btn-primary btn-sm my-3">Create Cast</a>
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Name</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Label</th>
            </tr>
        </thead>

        <tbody>
            @forelse ($cast as $cast)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $cast->name }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <button href="/cast/{{ $cast->id }}/edit" class="badge badge-success">Edit</button>
                        <button href="/cast/{{ $cast->id }}" class="badge badge-info">Detail</button>
                        <form action="/cast/{{ $cast->id }}" method="POST" class="d-inline">
                            @method('delete')
                            @csrf

                            <button class="badge badge-danger" type="submit" onclick="return confirm('Are you sure?')">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">Data tidak ditemukan</td>
                </tr>
            @endforelse
        </tbody>
    </table>


@endsection
@push('scripts')
    <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
