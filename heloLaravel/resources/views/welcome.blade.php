@extends('layouts.master')

@section('judul', 'Dashboard')

@section('content')

        <h1>SanberCode</h1>
        <h3>
            Social Media Developer Santai Berkualitas
        </h3>
        <p>
            Belajar dan Berbagi agar hidup ini semakin santai berkualitas
        </p>
        <h3>Benefit Join di SanberBook</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
        <h3>Cara Bergabung ke SanberBook</h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
            <li>Selesai!</li>
        </ol>
        <br>


        @endsection
        @push('scripts')
        <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
        <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
        <script>
          $(function () {
            $("#example1").DataTable();
          });
        </script>
        @endpush
        @push('styles')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
        @endpush