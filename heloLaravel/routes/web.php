<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);


Route::get('/data-table', function() {
    return view('pages.data-table');
});

Route::get('/table', function() {
    return view('pages.table');
});


// CRUD Cast

// Create Cast adalah halaman form untuk membuat data baru
Route::get('/cast/create', [CastController::class, 'create']);

// Store Cast adalah proses menyimpan data baru ke database
Route::post('/cast', [CastController::class, 'store']);

// Read Cast adalah halaman untuk menampilkan data
Route::get('/cast', [CastController::class, 'index']);

// Detail cast show the detail about case
Route::get('/cast/{cast}', [CastController::class, 'show']);

// Edit Cast adalah halaman form untuk mengubah data
Route::get('/cast/{cast}/edit', [CastController::class, 'edit']);

// Mengubah data ke database berdasarkan id
Route::put('/cast/{id}', [CastController::class, "update"]);

// Menghapus data dari database berdasarkan id
Route::delete('/cast/{id}', [CastController::class, "destroy"]);
