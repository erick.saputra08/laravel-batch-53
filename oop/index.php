<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");
echo "<br>Name: ";
echo $sheep->name; // "shaun"
echo "<br> Legs: ";
echo $sheep->legs; // 4
echo "<br> Cold Blooded: ";
echo $sheep->cold_blooded; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "<br> Name: ";
echo $kodok->name; // "buduk"
echo "<br> Legs: ";
echo $kodok->legs; // 4
echo "<br> Cold Blooded: ";
echo $kodok->cold_blooded; // "no"
echo "<br> Jump: ";
echo $kodok->jump(); // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "<br> Name: ";
echo $sungokong->name; // "kera sakti"
echo "<br> Legs: ";
echo $sungokong->legs; // 2
echo "<br> Cold Blooded: ";
echo $sungokong->cold_blooded; // "no"
echo "<br> Yell: ";
echo $sungokong->yell(); // "Auooo"
echo "<br>";


